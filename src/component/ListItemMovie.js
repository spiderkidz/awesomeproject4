import React, { Component } from 'react'
import { View, Text, StyleSheet, Image } from 'react-native'
import Constant from '../Constant'
import ButtonFavorite from '../component/ButtonFavorite'

export default class ListItemMovie extends Component {
  render() {
    const { item, addWatchList,removeWatchList } = this.props

    return (
        <View style={styles.rowContainer}>
            <Image
            style={styles.rowImage}
            source={{uri: Constant.api_image+item.poster_path}}
            />
            <View style={{marginLeft:5, flex:1,
                  alignItems:'flex-start'
                  }}>
            <Text style={styles.rowTitle}>
                {item.title}
            </Text>
            <Text style={styles.rowSubtitle}>
                {item.release_date}
            </Text>
            <ButtonFavorite item={item} addWatchList={addWatchList} removeWatchList={removeWatchList}
             style={{borderWidth:1, borderColor:'white'}}/>
            </View>
        </View>
    )
  }
}

const styles = StyleSheet.create({
    rowContainer:{
        borderWidth:1, 
        margin:10,
        backgroundColor:Constant.colors.lightGrey, 
        padding:10,
        flexDirection:'row',
      },
      rowTitle: {
        marginBottom: 5,
        color:'white',
        fontWeight:'bold'
      },
      rowSubtitle: {
        marginBottom: 5,
        color:'white',
      },
      rowImage: {
        height: 100, 
        width:60
      }
})
