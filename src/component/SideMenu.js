import PropTypes from 'prop-types';
import React, {Component} from 'react';
import {NavigationActions} from 'react-navigation';
import {ScrollView, Text, View, StyleSheet} from 'react-native';

export default class SideMenu extends Component {
    navigateToScreen = (route, param) => () => {
        console.log('clicked', route, param)
        const navigateAction = NavigationActions.navigate({
            routeName: route
        });
        this.props.navigation.replace(route, param);
    }
    
    render() {
        return (
            <View style={styles.container}>
                <View style={{height: 60}}></View>
                <ScrollView>
                    <View style={styles.rowItem}>
                        <Text style={styles.text} onPress={this.navigateToScreen('DrawerNav',{menu:'movie/top_rated'})}>
                        Top rated movies</Text>
                    </View>
                    <View style={styles.rowItem}>
                    <Text style={styles.text} onPress={this.navigateToScreen('DrawerNav',{menu:'movie/upcoming'})}>
                        Upcoming movies</Text>
                    </View>

                    <View style={styles.rowItem}>
                        <Text style={styles.text} onPress={this.navigateToScreen('DrawerNav',{menu:'movie/now_playing'})}>
                        Now playing movies</Text>
                    </View>
                    <View style={styles.rowItem}>
                        <Text style={styles.text} onPress={this.navigateToScreen('DrawerNav',{menu:'movie/popular'})}>
                        Popular movies</Text>
                    </View>
                    <View style={styles.rowItem}>
                        <Text style={styles.text} onPress={this.navigateToScreen('DrawerNavTv',{menu:'tv/popular'})}>
                        Popular TV shows</Text>
                    </View>
                    <View style={styles.rowItem}>
                        <Text style={styles.text} onPress={this.navigateToScreen('DrawerNavTv',{menu:'tv/top_rated'})}>
                        Top rated TV shows</Text>
                    </View>
                    <View style={styles.rowItem}>
                        <Text style={styles.text} onPress={this.navigateToScreen('DrawerNavTv',{menu:'tv/on_the_air'})}>
                        On the air TV shows</Text>
                    </View>
                    <View style={styles.rowItem}>
                        <Text style={styles.text} onPress={this.navigateToScreen('DrawerNavTv',{menu:'tv/airing_today'})}>
                        Airing today TV shows</Text>
                    </View>
                    <View style={styles.rowItem}>
                        <Text style={styles.text} onPress={this.navigateToScreen('DrawerNavPeople',{menu:'person/popular'})}>
                        Popular people</Text>
                    </View>
                    <View style={styles.rowItem}>
                        <Text style={styles.text} onPress={()=>{
                            this.props.navigation.push('ScreenWatchList');
                        }}>
                        Watch List</Text>
                    </View>
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#2f343a'
    }, text: {
        color: '#fff',
        fontSize: 20,
        fontWeight: 'bold'
    },
    rowItem: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        height: 50,
        marginLeft: 20
    }
});
SideMenu.propTypes = {
    navigation: PropTypes.object
};

