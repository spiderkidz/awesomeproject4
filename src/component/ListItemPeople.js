import React, { Component } from 'react'
import { View, Text, StyleSheet, Image } from 'react-native'
import Constant from '../Constant'

export default class ListItemPeople extends Component {
  render() {
    const { item } = this.props

    return (
        <View style={styles.rowContainer}>
            <Image
            style={styles.rowImage}
            source={{uri: Constant.api_image+item.profile_path}}
            />
            <View style={{marginLeft:5}}>
              <Text style={styles.rowTitle}>
                  {item.name}
              </Text>
            </View>
        </View>
    )
  }
}

const styles = StyleSheet.create({
    rowContainer:{
        borderWidth:1, 
        margin:10,
        backgroundColor:Constant.colors.lightGrey, 
        padding:10,
        flexDirection:'row'
      },
      rowTitle: {
        marginBottom: 5,
        color:'white',
        fontWeight:'bold'
      },
      rowSubtitle: {
        marginBottom: 5,
        color:'white',
      },
      rowImage: {
        height: 100, 
        width:60
      }
})
