import React, { Component } from 'react'
import { View, Text, StyleSheet } from 'react-native'
import Constant from '../Constant'

export default class Header extends Component {
  render() {
    const { children, navigation } = this.props
    console.log('header ', this.props)
    return (
      <View style={styles.header}>
        <Text style={{flex:2, color:'white'}} onPress={()=>{
            this.props.navigation.openDrawer();
        }}>Menu</Text>
        
        <Text style={styles.title}>{children}</Text>
        <Text style={{flex:2, color:'white'}} onPress={()=>{
            this.props.navigation.push('ScreenSearch');
        }}
        >Search</Text>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  header: {
    backgroundColor: Constant.colors.backgroundColor,
    padding: 15,
    flexDirection:'row',
    justifyContent:'space-around'
  },
  title: {
    textAlign: 'center',
    color: 'white',
    fontSize:20,
    fontWeight:'bold',
    flex:6
  },
})
