import React, { Component } from 'react'
import { View, Text, StyleSheet, Button } from 'react-native'
import Constant from '../Constant'

export default class ButtonFavorite extends Component {
  render() {
    const { item, addWatchList, removeWatchList } = this.props
    if(item.favorited){
        return (
            <Button
                onPress={()=>{
                    removeWatchList(item.id)
                }}
                title="Watchlisted"
                color={Constant.colors.ghostWhite}
                accessibilityLabel="Favorite"
            />
        )
    }else{
        return (
            <Button
                onPress={()=>{
                    addWatchList(item)
                    console.log('favorite')
                }}
                title="Watchlist"
                color={Constant.colors.blue}
                accessibilityLabel="Favorite"
            />
        )
    }
    
  }
}

const styles = StyleSheet.create({
  header: {
    backgroundColor: Constant.colors.backgroundColor,
    padding: 15,
    flexDirection:'row',
    justifyContent:'space-around'
  },
  title: {
    textAlign: 'center',
    color: 'white',
    fontSize:20,
    fontWeight:'bold',
    flex:6
  },
})
