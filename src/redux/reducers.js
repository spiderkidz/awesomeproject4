import {combineReducers} from 'redux';
import ReduxMovie from '../redux/ReduxMovie'
import { resettableReducer } from 'reduxsauce'

const resettable = resettableReducer('RESET')
const AppReducer = combineReducers({
    ReduxMovie:resettable(ReduxMovie)
});
export default AppReducer;