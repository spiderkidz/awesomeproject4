import {  createActions } from 'reduxsauce'
export const { Types, Creators } = createActions({
    addWatchlist: ['watchlist'],
    removeWatchlist: ['item'],
    addMovie: ['movielist'],
    updateMovielist: ['movielist'],
    // login:['username', 'password']
  }, {})