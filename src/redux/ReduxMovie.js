import { createReducer } from 'reduxsauce'
import types from './types'

  const initialState = {
    watchlist: [],
    movielist:[]
  }

  export const addMovie = (state = initialState, action) => {
    console.log('addMovie redux', state)
    return { ...state, movielist: action.movielist }
  }

  export const updateMovielist = (state = initialState, action) => {
    console.log('updateMovielist reducer', state)
    return { ...state, movielist: state.movielist.concat(action.movielist) }
  }

  export const addWatchlist = (state = initialState, action) => {
    console.log('addWatchlist reducer', state.watchlist, action.watchlist)
    return { ...state, watchlist: state.watchlist.concat(action.watchlist) }
  }

  export const removeWatchlist = (state = initialState, action) => {
    console.log('removeWatchlist redux', action.item, state.watchlist)
    console.log('remove xxx',state.watchlist.filter(item => item.id !== action.item))
    const rows=state.watchlist.filter(item => item.id !== action.item)
    return { 
      ...state, 
      watchlist: rows
    }
  }

  export const HANDLERS = {
    [types.ADD_WATCHLIST]: addWatchlist,
    [types.REMOVE_WATCHLIST]: removeWatchlist,
    [types.ADD_MOVIE]: addMovie,
    [types.UPDATE_MOVIELIST]: updateMovielist,
  }

  export default ReduxUser = createReducer(initialState, HANDLERS)
  
  