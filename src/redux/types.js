import { createTypes} from 'reduxsauce'

export default createTypes(`
ADD_WATCHLIST
REMOVE_WATCHLIST
ADD_MOVIE
UPDATE_MOVIELIST
`, {})