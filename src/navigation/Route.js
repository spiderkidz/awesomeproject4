import {
    createStackNavigator,
    createBottomTabNavigator,
    createDrawerNavigator,
    DrawerItems,
    SafeAreaView,
    createAppContainer
} from 'react-navigation';
import SplashScreen from '../screen/ScreenSplash'
import ScreenHome from '../screen/ScreenHome'
import ScreenTv from '../screen/ScreenTv'
import SideMenu from '../component/SideMenu'
import ScreenPeople from '../screen/ScreenPeople';
import ScreenSearch from '../screen/ScreenSearch';
import ScreenWatchList from '../screen/ScreenWatchList';

//   const DrawerNav = createDrawerNavigator({
//     ScreenHome: {
//       screen: ScreenHome,
//     }
//   });
  const DrawerNav = createDrawerNavigator({
    ScreenHome: ScreenHome
  }, {contentComponent: SideMenu});
  
  const DrawerNavTv = createDrawerNavigator({
    ScreenTv: ScreenTv
  }, {contentComponent: SideMenu});

  const DrawerNavPeople = createDrawerNavigator({
    ScreenPeople: ScreenPeople
  }, {contentComponent: SideMenu});
const RootStack = createStackNavigator({
    DrawerNav: {
        screen: DrawerNav,
        navigationOptions: {
            header: null //this will hide the header
        },
    },
    DrawerNavTv: {
        screen: DrawerNavTv,
        navigationOptions: {
            header: null //this will hide the header
        },
    },
    DrawerNavPeople: {
        screen: DrawerNavPeople,
        navigationOptions: {
            header: null //this will hide the header
        },
    },
    SplashScreen: {
        screen: SplashScreen,
        navigationOptions: {
            header: null 
        },
    },
    ScreenSearch: {
        screen: ScreenSearch,
        navigationOptions: {
            header: null 
        },
    },
    ScreenHome: {
        screen: ScreenHome,
        navigationOptions: {
            header: null 
        },
    },
    ScreenTv: {
        screen: ScreenTv,
        navigationOptions: {
            header: null 
        },
    },
    ScreenWatchList: {
        screen: ScreenWatchList,
        navigationOptions: {
            header: null 
        },
    },
},
{
    initialRouteName: "SplashScreen"
}
);
export default RootStack