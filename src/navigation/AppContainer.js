import Route from './Route';
import {createAppContainer} from 'react-navigation'

const AppContainer=createAppContainer(Route);
// export default AppNavigator
export {AppContainer, Route}