import React, { Component } from 'react';
import { View, FlatList, Text, StyleSheet, TouchableOpacity, RefreshControl, Image } from 'react-native';
import Constant from '../Constant'
import {getHttp} from '../services/HttpRequest'
import ListItemTv from '../component/ListItemTv'
import Header from '../component/Header'

let page=1
let canLoadmore=false
let menu='tv/popular';


const extractKey = ({id}) => id.toString()

export default class ScreenTv extends Component {
  state = {
    isRefreshing: true,
    rows:[]
  }

  componentDidMount(){
    // console.log('will mount', this.props)
    const { navigation } = this.props;
    menu = navigation.getParam('menu', 'tv/popular');
    this.onRefresh()
  }

  onEndReached = () => {
    console.log('onEndReached()')
    let url= Constant.api+menu+'?api_key='+Constant.api_key+'&language=en-US&page='+page
    if(canLoadmore){
      getHttp(url, ((isSuccess, responseJson)=>{
        if(isSuccess){
          // const {dispatch, users} = this.props
          let data=responseJson.results
          // dispatch(Creators.addUser(data))
          console.log('data masuk', data)
          this.setState({
            isRefreshing: false,
            rows:this.state.rows.concat(data)
          })
          page+=1
          if(responseJson.results.length==0){
            canLoadmore=false
          }
        }else{
          console.log('eror endreach')
          this.setState({isRefreshing: false})
        }
      }))
    }
  }

  onRefresh = () =>{
    this.setState({isRefreshing: true})
    page=1
    let url= Constant.api+menu+'?api_key='+Constant.api_key+'&language=en-US&page='+page
    getHttp(url, (isSuccess, responseJson)=>{
      if(isSuccess){
        console.log('onrefresh')

        // const {dispatch} = this.props
        let data=responseJson.results
        // dispatch(Creators.addUser(data))
        console.log(data)
        this.setState({
          isRefreshing: false,
          rows:data
        })
        page+=1
        canLoadmore=true
      }else{
        this.setState({isRefreshing: false})
      }
    })
  }

  
  renderItem = ({item}) => {
    return (
        <ListItemTv item={item} />
    )
  }
  
  render() {
    let {isRefreshing, rows} = this.state
    return (
      <View style={styles.container}>
        <Header navigation={this.props.navigation}>
          Movie Apps
        </Header>
        <FlatList
        data={rows}
        renderItem={this.renderItem}
        keyExtractor={extractKey}
        onEndReached={this.onEndReached}
        onEndReachedThreshold={0.5}
        refreshControl={
          <RefreshControl
            refreshing={isRefreshing}
            onRefresh={this.onRefresh}
          />
        }
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    marginTop: 20,
    flex: 1,
    backgroundColor:Constant.colors.backgroundColor
  },
  rowContainer:{
    borderWidth:1, 
    margin:10,
    backgroundColor:Constant.colors.lightGrey, 
    padding:10,
    flexDirection:'row'
  },
  rowTitle: {
    marginBottom: 5,
    color:'white',
    fontWeight:'bold'
  },
  rowSubtitle: {
    marginBottom: 5,
    color:'white',
  },
  rowImage: {
    height: 100, 
    width:60
  }
})
