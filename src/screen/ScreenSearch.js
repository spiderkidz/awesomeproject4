import React, { Component } from 'react';
import { View, FlatList, Text, StyleSheet, RefreshControl } from 'react-native';
import Constant from '../Constant'
import {getHttp} from '../services/HttpRequest'
import ListItemMovie from '../component/ListItemMovie'
import Input from '../component/Input'
import ListItemPeople from '../component/ListItemPeople';
import ListItemTv from '../component/ListItemTv';
import { connect } from 'react-redux'
import { Creators } from '../redux/Actions'
let page=1
let canLoadmore=false
let query = ''


const extractKey = ({id}) => id.toString()
const mapStateToProps = (state) => ({
  watchlist: state.ReduxMovie.watchlist,
  movielist: state.ReduxMovie.movielist
})
class ScreenSearch extends Component {
  state = {
    isRefreshing: false,
    rows:[]
  }

  componentDidMount(){
  }

  onEndReached = () => {
    console.log('onEndReached()')
    let url= Constant.api+'search/multi?api_key='+Constant.api_key+'&language=en-US&page='+page+'&include_adult=false&language=en-US&query='+query
    if(canLoadmore){
      getHttp(url, ((isSuccess, responseJson)=>{
        if(isSuccess){
          // const {dispatch, users} = this.props
          let data=responseJson.results
          // dispatch(Creators.addUser(data))
          console.log('data masuk', data)
          this.setState({
            isRefreshing: false,
            rows:this.state.rows.concat(data)
          })
          page+=1
          if(responseJson.results.length==0){
            canLoadmore=false
          }
        }else{
          console.log('eror endreach')
          this.setState({isRefreshing: false})
        }
      }))
    }
  }

  onRefresh = (keyword) =>{
    query = keyword
    this.setState({isRefreshing: true})
    page=1
    let url= Constant.api+'search/multi?api_key='+Constant.api_key+'&language=en-US&page='+page+'&include_adult=false&language=en-US&query='+query
    getHttp(url, (isSuccess, responseJson)=>{
      if(isSuccess){
        console.log('onrefresh')

        // const {dispatch} = this.props
        let data=responseJson.results
        // dispatch(Creators.addUser(data))
        console.log(data)
        this.setState({
          isRefreshing: false,
          rows:this.state.rows.concat(data)
        })
        page+=1
        canLoadmore=true
      }else{
        this.setState({isRefreshing: false})
      }
    })
  }

  addWatchList=(id)=>{
    const {dispatch} = this.props
    console.log('addWatchList', id)
    dispatch(Creators.addWatchlist(id))
  }

  removeWatchList = (item)=>{
    const {dispatch} = this.props
    dispatch(Creators.removeWatchlist(item))
  }

  renderItem = ({item}) => {
    if(item.media_type==='tv'){
      return (
        <ListItemTv item={item}/>
      )
    }else if(item.media_type==='movie'){
      const { watchlist } = this.props;
      if(watchlist.length==0){
        item.favorited=false
      }
      for (var i = 0; i < watchlist.length; i++){
        // look for the entry with a matching `code` value
        if (watchlist[i].id == item.id){
           // we found it
          console.log('render item ada',item.id)
          item.favorited=true
          break;
        }
        if (watchlist[i].id != item.id){
          console.log('render item xxx', item.id)
          item.favorited=false
        }
      }
      return (
          <ListItemMovie item={item} addWatchList={this.addWatchList} removeWatchList={this.removeWatchList}/>
      )
    }else{
      return (
        <ListItemPeople item={item}/>
      )
    }
  }
  
  render() {
    let {isRefreshing, rows} = this.state
    return (
      <View style={styles.container}>
        <Input
          placeholder={'Search movie, tv, or people, then hit enter!'}
          onSubmitEditing={this.onRefresh}
        />
        <FlatList
        data={rows}
        renderItem={this.renderItem}
        keyExtractor={extractKey}
        onEndReached={this.onEndReached}
        onEndReachedThreshold={0.5}
        refreshControl={
          <RefreshControl
            refreshing={isRefreshing}
            onRefresh={this.onRefresh}
          />
        }
        />
      </View>
    );
  }
}

export default connect(mapStateToProps)(ScreenSearch)

const styles = StyleSheet.create({
  container: {
    marginTop: 20,
    flex: 1,
    backgroundColor:Constant.colors.backgroundColor
  },
  rowContainer:{
    borderWidth:1, 
    margin:10,
    backgroundColor:Constant.colors.lightGrey, 
    padding:10,
    flexDirection:'row'
  },
  rowTitle: {
    marginBottom: 5,
    color:'white',
    fontWeight:'bold'
  },
  rowSubtitle: {
    marginBottom: 5,
    color:'white',
  },
  rowImage: {
    height: 100, 
    width:60
  }
})
