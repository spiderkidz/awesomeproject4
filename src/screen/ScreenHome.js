import React, { Component } from 'react';
import { SafeAreaView, View, FlatList, Text, StyleSheet, TouchableOpacity, RefreshControl, Image } from 'react-native';
import Constant from '../Constant'
import {getHttp} from '../services/HttpRequest'
import Header from '../component/Header'
import ListItemMovie from '../component/ListItemMovie'
import { connect } from 'react-redux'
import { Creators } from '../redux/Actions'

let page=1
let canLoadmore=false
let menu='movie/top_rated';


const extractKey = ({id}) => id.toString()

const mapStateToProps = (state) => ({
  watchlist: state.ReduxMovie.watchlist,
  movielist: state.ReduxMovie.movielist
})

class ScreenHome extends Component {
  state = {
    isRefreshing: true,
  }

  componentDidMount(){
    // console.log('will mount', this.props)
    const { navigation, watchlist, movielist } = this.props;
    menu = navigation.getParam('menu', 'movie/top_rated');
    this.onRefresh()
    console.log('watchlist', watchlist)
    console.log('movielist', movielist)
  }

  onEndReached = () => {
    console.log('onEndReached()')
    let url= Constant.api+menu+'?api_key='+Constant.api_key+'&language=en-US&page='+page
    if(canLoadmore){
      getHttp(url, ((isSuccess, responseJson)=>{
        if(isSuccess){
          
          let data=responseJson.results
          const {dispatch} = this.props
          dispatch(Creators.updateMovielist(data))
          console.log('data masuk', data)
          
          this.setState({isRefreshing: false})
          page+=1
          if(responseJson.results.length==0){
            canLoadmore=false
          }
        }else{
          console.log('eror endreach')
          this.setState({isRefreshing: false})
        }
      }))
    }
  }

  onRefresh = () =>{
    this.setState({isRefreshing: true})
    page=1
    let url= Constant.api+menu+'?api_key='+Constant.api_key+'&language=en-US&page='+page
    getHttp(url, (isSuccess, responseJson)=>{
      if(isSuccess){
        console.log('onrefresh')

        let data=responseJson.results
        const {dispatch} = this.props
        dispatch(Creators.addMovie(data))
        console.log(data)
        this.setState({isRefreshing: false})
        page+=1
        canLoadmore=true
      }else{
        this.setState({isRefreshing: false})
      }
    })
  }

  addWatchList=(id)=>{
    const {dispatch} = this.props
    console.log('addWatchList', id)
    dispatch(Creators.addWatchlist(id))
  }

  removeWatchList = (item)=>{
    const {dispatch} = this.props
    dispatch(Creators.removeWatchlist(item))
  }
  
  renderItem = ({item}) => {
    const { watchlist } = this.props;
    console.log(watchlist.some(x => x.id === item.id));
    if(watchlist.some(x => x.id === item.id)){
      item.favorited=true
    }else{
      item.favorited=false
    }
    return (
        <ListItemMovie item={item} addWatchList={this.addWatchList} removeWatchList={this.removeWatchList}/>
    )
  }
  
  render() {
    let {isRefreshing} = this.state
    const { movielist } = this.props;
    console.log('render', movielist)
    return (
      <View style={styles.container}>
        <Header navigation={this.props.navigation}>
          Movie Apps
        </Header>
        <FlatList
        data={movielist}
        renderItem={this.renderItem}
        keyExtractor={extractKey}
        onEndReached={this.onEndReached}
        onEndReachedThreshold={0.5}
        refreshControl={
          <RefreshControl
            refreshing={isRefreshing}
            onRefresh={this.onRefresh}
          />
        }
        />
      </View>
    );
  }
}
export default connect(mapStateToProps)(ScreenHome)
const styles = StyleSheet.create({
  container: {
    marginTop: 20,
    flex: 1,
    backgroundColor:Constant.colors.backgroundColor
  },
  rowContainer:{
    borderWidth:1, 
    margin:10,
    backgroundColor:Constant.colors.lightGrey, 
    padding:10,
    flexDirection:'row'
  },
  rowTitle: {
    marginBottom: 5,
    color:'white',
    fontWeight:'bold'
  },
  rowSubtitle: {
    marginBottom: 5,
    color:'white',
  },
  rowImage: {
    height: 100, 
    width:60
  }
})
