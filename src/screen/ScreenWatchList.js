import React, { Component } from 'react';
import { SafeAreaView, View, FlatList, Text, StyleSheet, TouchableOpacity, RefreshControl, Image } from 'react-native';
import Constant from '../Constant'
import ListItemMovie from '../component/ListItemMovie'
import { connect } from 'react-redux'
import { Creators } from '../redux/Actions'

const extractKey = ({id}) => id.toString()

const mapStateToProps = (state) => ({
  watchlist: state.ReduxMovie.watchlist,
  movielist: state.ReduxMovie.movielist
})

class ScreenHome extends Component {
  state = {
    isRefreshing: false,
  }

  componentDidMount(){
    // console.log('will mount', this.props)
    const { navigation, watchlist, movielist } = this.props;
    // this.onRefresh()
    console.log('watchlist', watchlist)
    console.log('movielist', movielist)
  }

  onEndReached = () => {
    console.log('onEndReached()')
    
  }

  addWatchList=(id)=>{
    const {dispatch} = this.props
    console.log('addWatchList', id)
    dispatch(Creators.addWatchlist(id))
  }

  removeWatchList = (item)=>{
    const {dispatch} = this.props
    dispatch(Creators.removeWatchlist(item))
  }
  
  renderItem = ({item}) => {
    console.log('render item ID',item.id)
    item.favorited=true
    return (
        <ListItemMovie item={item} addWatchList={this.addWatchList} removeWatchList={this.removeWatchList}/>
    )
  }
  
  render() {
    let {isRefreshing} = this.state
    const { watchlist } = this.props;
    console.log('render', watchlist)
    return (
      <View style={styles.container}>
        <FlatList
        data={watchlist}
        renderItem={this.renderItem}
        keyExtractor={extractKey}
        onEndReached={this.onEndReached}
        onEndReachedThreshold={0.5}
        refreshControl={
          <RefreshControl
            refreshing={isRefreshing}
            onRefresh={this.onRefresh}
          />
        }
        />
      </View>
    );
  }
}
export default connect(mapStateToProps)(ScreenHome)
const styles = StyleSheet.create({
  container: {
    marginTop: 20,
    flex: 1,
    backgroundColor:Constant.colors.backgroundColor
  },
  rowContainer:{
    borderWidth:1, 
    margin:10,
    backgroundColor:Constant.colors.lightGrey, 
    padding:10,
    flexDirection:'row'
  },
  rowTitle: {
    marginBottom: 5,
    color:'white',
    fontWeight:'bold'
  },
  rowSubtitle: {
    marginBottom: 5,
    color:'white',
  },
  rowImage: {
    height: 100, 
    width:60
  }
})
