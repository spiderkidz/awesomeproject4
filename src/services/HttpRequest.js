import Constant from '../Constant'

async function getHttp(url, callback){
    try {
      console.log('url'+url)
      const response = await fetch(url)
      const responseJson = await response.json()

      console.log('getHttp',responseJson.results)
      
      callback(true, responseJson)
    } catch (e) {
      console.log('error', e)
      callback(false)
    }
  }

  export { getHttp };