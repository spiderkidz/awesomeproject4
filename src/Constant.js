import {Platform} from 'react-native';

let ENV = 'prod'; // dev or prod
let API = 'https://api.themoviedb.org/3/'
let API_KEY = '9c031ab1e36fcae53291e6430e68498c'
let API_IMAGE = 'https://image.tmdb.org/t/p/w300/'

module.exports.api = API
module.exports.api_key = API_KEY
module.exports.api_image = API_IMAGE

module.exports.Platform = Platform

module.exports.colors = {
    backgroundColor: "#15191C",
    backgroundLabelColor: "#192229",
    red: '#CA0F1A',
    orange: '#F48227',
    buttonColor: '#4F5761',
    lightGrey: '#2F343A',
    darkGrey: '#23262B',
    grey: '#616161',
    lightGrey2: '#4F5761',
    green:'#68BA4D',
    blue:'#4A98F6',
    yellow:'#FEE65E',
    pink:'#FC1737',
    ghostWhite:'#f7f9f9'
}