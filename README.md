**How to run:**

* Pull the source code from master branch
* Go to the root project directory
* Install package dependency `yarn install` or `npm install`
* Run the app `react-native run-ios` or for android `react-native run-android` this will open the simulator
* For running on device plese refer to https://facebook.github.io/react-native/docs/running-on-device
* You can download the ready apk [here](https://drive.google.com/file/d/1eDx2L9R8IZ9ohDNXBNnAvQa8fvFOoVi_/view?usp=sharing). make sure you allow your phone to install non playstore apps.

