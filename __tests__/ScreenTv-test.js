/**
 * @format
 */

import React from 'react';
import ScreenHome from '../src/screen/ScreenTv';

// Note: test renderer must be required after react-native.
import renderer from 'react-test-renderer';

it('renders correctly', () => {
    const navigate = { getParam: jest.fn() };
    navigate.getParam.mockReturnValue('tv/popular');
    fetch = jest.fn(() => Promise.resolve());
    // console.log('nav', navigate.getParam());  
    const tree = renderer.create(<ScreenHome navigation={navigate}/>).toJSON();
    expect(tree).toMatchSnapshot();
});
