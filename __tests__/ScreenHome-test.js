/**
 * @format
 */

import {View} from 'react-native';
import React from 'react';
import ScreenHome from '../src/screen/ScreenHome';
import { createStore, applyMiddleware, combineReducers } from 'redux';
// Note: test renderer must be required after react-native.
import renderer from 'react-test-renderer';

it('renders correctly', () => {
    const navigate = { getParam: jest.fn() };
    navigate.getParam.mockReturnValue('movie/upcoming');
    fetch = jest.fn(() => Promise.resolve());
    // console.log('nav', navigate.getParam());  
    const tree = renderer.create(<ScreenHome navigation={navigate}/>).toJSON();
    expect(tree).toMatchSnapshot();
});