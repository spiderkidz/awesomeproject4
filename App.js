import React, {Component} from 'react';
import {AppContainer} from './src/navigation/AppContainer';
import { createStore } from 'redux'
import { Provider } from 'react-redux'
import AppReducer from './src/redux/reducers';
import { persistStore, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage' // defaults to localStorage for web and AsyncStorage for react-native
const persistConfig = {
  key: 'root',
  storage,
}
const persistedReducer = persistReducer(persistConfig, AppReducer)
const store = createStore(persistedReducer)
// Enable persistence
persistStore(store)



export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <AppContainer/>
        </Provider>
    );
  }
}

